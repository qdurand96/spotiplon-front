import { configureStore, ThunkAction, Action } from '@reduxjs/toolkit';
import albumSlice from './album-slice';

export const store = configureStore({
  reducer: {
    album: albumSlice
  },
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
