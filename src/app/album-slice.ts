import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios';
import {Album} from '../entities'
import { AppDispatch } from './store';

interface AlbumState {
    list:Album[],
    selected:Album
}

const initialState: AlbumState = {
    list:[],
    selected:{}
}

const albumSlice = createSlice({
    name: 'album',
    initialState,
    reducers: {
        setAlbums(state,{payload}){
            state.list = payload;
        },
        selectAlbum(state,{payload}){
            state.selected = payload;
        }
    }
})

export const { setAlbums, selectAlbum } = albumSlice.actions

export default albumSlice.reducer

export const fetchAlbums = () => async(dispatch:AppDispatch) => {
    try {
        const response = await axios.get('/api/album')
        dispatch(setAlbums(response.data))
    } catch (error) {
        console.log(error);
    }
}

export const fetchSelected = (id:string) => async(dispatch:AppDispatch)=>{
    try {
        const response = await axios.get('/api/album/'+id);
        dispatch(selectAlbum(response.data))
    } catch (error) {
        console.log(error);
    }
}