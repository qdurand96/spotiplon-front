export interface Album {
    id?:number;
    title?:string;
    image?:string;
    released?:string;
    songs?:Song[];
    artists?:Artist[]
}

export interface Artist {
    id?:number;
    name?:string;
    image?:string;
    albums?:Album[];
    songs?:Song[];
}

export interface Song {
    id?:number;
    title?:string;
    file?:string;
    released?:string;
    artists?:Artist[];
    albums?:Album[];
}