import { Link } from "react-router-dom";
import { Album } from "../entities";

interface Props {
    album:Album
}

export function ListedAlbum({album}:Props){


    return(
        <>
            <p>{album.title}</p>
            {album.artists?.map(item=> <p key={item.id}>{item.name}</p>)}
            <p>{album.released}</p>
            <img src={album.image} alt="album" />
            <Link to={'/album/'+album.id}>Details</Link>
        </>
    )
}