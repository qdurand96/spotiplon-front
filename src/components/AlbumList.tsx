import { useEffect } from "react";
import { fetchAlbums } from "../app/album-slice";
import { useAppDispatch, useAppSelector } from "../app/hooks";
import { ListedAlbum } from "./ListedAlbum";


export function AlbumList() {


    const dispatch = useAppDispatch()

    useEffect(()=>{
        dispatch(fetchAlbums())
    },[dispatch])

    const albums = useAppSelector(state => state.album.list)

    return (
        <>  
            {albums.map(item => <ListedAlbum key={item.id} album={item}/> )} 
        </>
    )
}