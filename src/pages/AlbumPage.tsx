import { useEffect } from "react"
import { useParams } from "react-router-dom"
import { fetchSelected } from "../app/album-slice"
import { useAppDispatch, useAppSelector } from "../app/hooks"


export function AlbumPage() {

    interface Param {
        id: string
    }

    const { id } = useParams<Param>()

    const dispatch = useAppDispatch()

    useEffect(() => {
        
        dispatch(fetchSelected(id))
    }, [dispatch,id])

    const album = useAppSelector(state => state.album.selected)

    return (
        <> 
            <p>{album.title}</p>
            {album.artists?.map(item => <p key={item.id}>{item.name}</p>)}
            <p>{album.released}</p>
            <img src={album.image} alt="album" />
            {album.songs?.map(item=> <p key={item.id}>{item.title}</p> )}
        </>
    )
}