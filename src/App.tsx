import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { AlbumPage } from './pages/AlbumPage';
import { HomePage } from './pages/HomePage';

function App() {
  return (
    <BrowserRouter>
      <Switch>

        <Route exact path='/'>
          <HomePage />
        </Route>

        <Route path='/album/:id' children={<AlbumPage/>} />

      </Switch>
    </BrowserRouter>
  );
}

export default App;
